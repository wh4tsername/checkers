begin_task()

task_link_libraries(whirl-frontend)

# Task libraries
add_task_library(server atomic-server)
add_task_library(client atomic-client)

# Tests
task_link_libraries(checkers)
add_task_test_dir(main/ main)

end_task()
