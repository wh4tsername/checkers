// Simulation
#include <checkers/world/world.hpp>
#include <checkers/world/global/vars.hpp>
#include <checkers/world/global/time.hpp>
#include <checkers/test/random.hpp>
#include <checkers/test/main.hpp>
#include <checkers/test/event_log.hpp>
#include <checkers/test/runner.hpp>
#include <checkers/checker/checker.hpp>

#include <client/client.hpp>
#include <server/server.hpp>

using namespace whirl;

// Seed -> simulation digest
// Deterministic
void RunSimulation(size_t seed, bool explore) {
  auto& runner = checkers::TestRunner::Access();

  runner.Report() << "Simulation seed: " << seed << std::endl;

  checkers::Random random{seed};

  // Randomize simulation parameters
  const size_t servers = 1;
  const size_t clients = 2;

  // TODO pass as arg
  std::string main_log("/home/wasd/thesis/checkers/logs/log.txt");
  checkers::World world{seed, main_log};

  world.SetupServers(servers, atomic::Server);
  world.SetupClients(clients, atomic::Client);

  // Globals
  world.SetGlobal("requests", 0ul);

  // Explore
  checkers::Checker checker(world);

  if (explore) {
    auto trace = checker.Explore(/*steps=*/-1);

    if (!trace.empty()) {
      checker.RunBranch(trace);

      runner.Fail();
    }
  } else {
    checker.RunFirstBranch();
  }

  size_t completed = std::any_cast<size_t>(world.GetGlobal("requests"));

  runner.Report() << "Requests completed: " << completed << std::endl;
}

int main(int argc, const char** argv) {
  return checkers::Main(argc, argv, RunSimulation);
}
