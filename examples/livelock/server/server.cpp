#include <server/server.hpp>

// Node
#include <whirl/node/runtime/shortcuts.hpp>
#include <whirl/node/rpc/server.hpp>
#include <whirl/node/cluster/peer.hpp>

// Concurrency
#include <await/fibers/core/api.hpp>
#include <await/fibers/sync/future.hpp>
#include <await/futures/util/never.hpp>

// RPC
#include <commute/rpc/id.hpp>
#include <commute/rpc/service_base.hpp>
#include <commute/rpc/call.hpp>

#include <fstream>

namespace atomic {

using await::futures::Future;
using namespace whirl;

class Atomic : public commute::rpc::ServiceBase<Atomic>,
               public node::cluster::Peer {
 public:
  using Integer = int64_t;

  explicit Atomic()
      : Peer(node::rt::Config()), logger_("Atomic", node::rt::LoggerBackend()) {
  }

  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Load);
    COMMUTE_RPC_REGISTER_METHOD(Store);
    COMMUTE_RPC_REGISTER_METHOD(CompareExchange);
  };

  // RPC handlers
  Integer Load() {
    return value_.load();
  }

  void Store(Integer value) {
    value_.store(value);
  }

  Integer CompareExchange(Integer expected, Integer desired) {
    value_.compare_exchange_strong(expected, desired);

    return expected;
  }

 private:
  timber::Logger logger_;

  twist::stdlike::atomic<Integer> value_{0};
};

void Server() {
  // Start RPC server

  auto rpc_port = node::rt::Config()->GetInt<uint16_t>("rpc.port");
  auto rpc_server = node::rpc::MakeServer(rpc_port);

  rpc_server->RegisterService("Atomic", std::make_shared<Atomic>());

  rpc_server->Start();

  // Serving ...

  await::futures::BlockForever();
}

}  // namespace atomic
