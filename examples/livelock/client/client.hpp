#pragma once

// Node
#include <whirl/node/runtime/shortcuts.hpp>
#include <whirl/node/rpc/server.hpp>
#include <whirl/node/cluster/peer.hpp>

// Concurrency
#include <await/fibers/core/api.hpp>
#include <await/fibers/sync/future.hpp>

// RPC
#include <commute/rpc/id.hpp>
#include <commute/rpc/service_base.hpp>
#include <commute/rpc/call.hpp>
#include <commute/rpc/client.hpp>

// Logging
#include <timber/log.hpp>

// Formatting
#include <fmt/core.h>

namespace atomic {

using await::futures::Future;
using namespace whirl;

std::string GenerateTraceId(std::string op) {
  return fmt::format("{}-{}", op, whirl::node::rt::GenerateGuid());
}

class AtomicStub {
 public:
  using Integer = int64_t;

  explicit AtomicStub(commute::rpc::IChannelPtr channel) : channel_(channel) {
  }

  void Store(Integer value) {
    auto future = commute::rpc::Call("Atomic.Store")
                      .Args(value)
                      .Via(channel_)
                      .TraceWith(GenerateTraceId("Store"))
                      .Start()
                      .As<void>();

    await::fibers::Await(std::move(future)).ThrowIfError();
  }

  Integer Load() {
    auto future = commute::rpc::Call("Atomic.Load")
                      .Args()
                      .Via(channel_)
                      .TraceWith(GenerateTraceId("Load"))
                      .Start()
                      .As<Integer>();

    return await::fibers::Await(std::move(future)).ValueOrThrow();
  }

  Integer CompareExchange(Integer expected, Integer desired) {
    return await::fibers::Await(
               commute::rpc::Call("Atomic.CompareExchange")
                   .Args(expected, desired)
                   .Via(channel_)
                   .TraceWith(GenerateTraceId("CompareExchange"))
                   .Start()
                   .As<Integer>())
        .ValueOrThrow();
  }

  Integer FetchAdd(Integer value) {
    while (true) {
      Integer old = Load();

      if (CompareExchange(old, old + value) == old) {
        return old;
      }
    }
  }

  Integer FetchSub(Integer value) {
    while (true) {
      Integer old = Load();

      if (CompareExchange(old, old - value) == old) {
        return old;
      }
    }
  }

 private:
  commute::rpc::IChannelPtr channel_;
};

void Client() {
  await::fibers::self::SetName("main");

  timber::Logger logger{"Client", node::rt::LoggerBackend()};

  auto client =
      ::commute::rpc::MakeClient(node::rt::NetTransport(), node::rt::Executor(),
                                 node::rt::LoggerBackend());

  auto channel =
      client->Dial(fmt::format("{}:{}", /*host=*/"server-1", /*port=*/42));

  AtomicStub atomic(channel);

  {
    while (atomic.FetchAdd(1) > 0) {
      atomic.FetchSub(1);  // Step back
    }
  }

  { atomic.FetchSub(1); }

  checkers::GlobalCounter("requests").Increment();
}

}  // namespace atomic
