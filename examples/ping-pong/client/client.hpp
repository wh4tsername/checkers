#pragma once

#include <client/predicate.hpp>

// Node
#include <whirl/node/runtime/shortcuts.hpp>

// RPC
#include <commute/rpc/id.hpp>
#include <commute/rpc/service_base.hpp>
#include <commute/rpc/call.hpp>

// Concurrency
#include <await/fibers/core/api.hpp>
#include <await/fibers/sync/future.hpp>
#include <await/futures/util/never.hpp>

// TODO add to runtime
#include <checkers/world/world.hpp>

namespace ping_pong {

using await::futures::Future;
using namespace whirl;

class BlockingClient {
 public:
  explicit BlockingClient(commute::rpc::IChannelPtr channel)
      : channel_(channel) {
  }

  void Init() {
    await::fibers::Await(commute::rpc::Call("PingPong.Init")
                             .Args()
                             .Via(channel_)
                             .TraceWith(GenerateTraceId("Init"))
                             .Start()
                             .As<void>())
        .ThrowIfError();
  }

 private:
  std::string GenerateTraceId(std::string op) const {
    return fmt::format("{}-{}", op, whirl::node::rt::GenerateGuid());
  }

 private:
  commute::rpc::IChannelPtr channel_;
};

void Client() {
  await::fibers::self::SetName("main");

  timber::Logger logger_{"Client", node::rt::LoggerBackend()};

  auto client =
      ::commute::rpc::MakeClient(node::rt::NetTransport(), node::rt::Executor(),
                                 node::rt::LoggerBackend());

  auto channel =
      client->Dial(fmt::format("{}:{}", /*host=*/"server-1", /*port=*/42));

  BlockingClient stub{channel};

  // TODO
  auto predicate = whirl::checkers::ThisWorld()->GetPredicate();

  //  for (size_t index = 0; index < 2; ++index) {
  LOG_INFO("Init PingPong");
  stub.Init();
  LOG_INFO("Done PingPong");

  //    predicate->Fail("");

  checkers::GlobalCounter("requests").Increment();
  //  }
}

}  // namespace ping_pong
