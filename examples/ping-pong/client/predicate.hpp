#pragma once

#include <checkers/predicate/predicate.hpp>

using namespace whirl;

namespace ping_pong {

class PingPongPredicate : public checkers::IPredicate {
 public:
  virtual ~PingPongPredicate() = default;

  void Fail([[maybe_unused]] const std::string& reason) override {
    is_failed_ = true;
  }
  void Report([[maybe_unused]] const std::string& state) override {
  }

  bool IsCorrect() override {
    return !is_failed_;
  }
  bool MakesSense() override {
    return true;
  }

  void Reset() override {
    is_failed_ = false;
  }

 private:
  bool is_failed_{false};
};

}  // namespace ping_pong
