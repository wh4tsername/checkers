// Node
#include <whirl/node/runtime/shortcuts.hpp>
#include <whirl/node/rpc/server.hpp>
#include <whirl/node/cluster/peer.hpp>

// Concurrency
#include <await/fibers/core/api.hpp>
#include <await/futures/util/never.hpp>

// RPC
#include <commute/rpc/id.hpp>
#include <commute/rpc/service_base.hpp>
#include <commute/rpc/call.hpp>

// Logging
#include <timber/log.hpp>

#include <server/server.hpp>

namespace ping_pong {

using await::futures::Future;
using namespace whirl;

class PingPong : public commute::rpc::ServiceBase<PingPong>,
                 public node::cluster::Peer {
 public:
  PingPong()
      : Peer(node::rt::Config()),
        logger_("PingPong", node::rt::LoggerBackend()) {
  }

  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Init);
    COMMUTE_RPC_REGISTER_METHOD(Ping);
    COMMUTE_RPC_REGISTER_METHOD(Pong);
  };

  // RPC handlers

  void Init() {
    // Broadcast
    SelfBroadcast("Ping");
  }

  void Ping() {
    if (pong_count_.load() < kPongThreshold) {
      LOG_INFO("Ping");

      SelfBroadcast("Pong");
      pong_count_.fetch_add(1);
    }
  }

  void Pong() {
    if (ping_count_.load() < kPingThreshold) {
      LOG_INFO("Pong");

      SelfBroadcast("Ping");
      ping_count_.fetch_add(1);
    }
  }

 private:
  void SelfBroadcast(const std::string& method) {
    for (const auto& peer : ListPeers().WithoutMe()) {
      if (peer.starts_with("server")) {
        Future<void> future = commute::rpc::Call("PingPong." + method)
                                  .Args()
                                  .Via(Channel(peer))
                                  .Context(await::context::ThisFiber())
                                  .AtLeastOnce();
      }
    }
  }

  timber::Logger logger_;

  twist::stdlike::atomic<size_t> ping_count_{0};
  twist::stdlike::atomic<size_t> pong_count_{0};
};

void Server() {
  // Start RPC server

  auto rpc_port = node::rt::Config()->GetInt<uint16_t>("rpc.port");
  auto rpc_server = node::rpc::MakeServer(rpc_port);

  rpc_server->RegisterService("PingPong", std::make_shared<PingPong>());

  rpc_server->Start();

  // Serving ...

  await::futures::BlockForever();
}

}  // namespace ping_pong
