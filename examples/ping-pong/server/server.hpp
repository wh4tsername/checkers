#pragma once

namespace ping_pong {

static const size_t kPingThreshold = 2;
static const size_t kPongThreshold = 2;

void Server();

}  // namespace ping_pong
