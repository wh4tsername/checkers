// Node
#include <whirl/node/rpc/server.hpp>
#include <whirl/node/cluster/peer.hpp>

// Simulation
#include <checkers/world/world.hpp>
#include <checkers/world/global/vars.hpp>
#include <checkers/test/random.hpp>
#include <checkers/test/main.hpp>
#include <checkers/test/runner.hpp>
#include <checkers/checker/checker.hpp>

#include <client/client.hpp>
#include <client/predicate.hpp>
#include <server/server.hpp>

using namespace whirl;

// Deterministic
void RunSimulation(size_t seed, bool explore) {
  auto& runner = checkers::TestRunner::Access();

  runner.Report() << "Simulation seed: " << seed << std::endl;

  checkers::Random random{seed};

  // Randomize simulation parameters
  const size_t servers = 2;
  const size_t clients = 1;

  // Reset RPC ids
  commute::rpc::ResetIds();

  // TODO pass as arg
  std::string main_log("/home/wasd/thesis/checkers/logs/log.txt");
  checkers::World world{seed, main_log};

  world.SetupServers(servers, ping_pong::Server);
  world.SetupClients(clients, ping_pong::Client);

  // Predicate
  world.SetPredicate(std::make_shared<ping_pong::PingPongPredicate>());

  // Globals
  world.SetGlobal("requests", 0ul);

  // Explore
  checkers::Checker checker(world);

  if (explore) {
    auto trace = checker.Explore(/*steps=*/-1);

    if (!trace.empty()) {
      checker.RunBranch(trace);

      runner.Fail();
    }
  } else {
    checker.RunFirstBranch();
  }

  size_t completed = std::any_cast<size_t>(world.GetGlobal("requests"));

  runner.Report() << "Requests completed: " << completed << std::endl;
}

int main(int argc, const char** argv) {
  return checkers::Main(argc, argv, RunSimulation);
}
