begin_task()

task_link_libraries(whirl-frontend)

# Task libraries
add_task_library(server ping_pong-server)
add_task_library(client ping_pong-client)

# Tests
task_link_libraries(checkers)
add_task_test_dir(main/ main)

end_task()
