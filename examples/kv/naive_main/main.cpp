#include <naive/server.hpp>
#include <client/routine.hpp>
#include <client/types.hpp>
#include <client/predicate.hpp>

// Simulation
#include <checkers/world/world.hpp>
#include <checkers/test/main.hpp>
#include <checkers/test/runner.hpp>
#include <checkers/checker/checker.hpp>

using namespace whirl;

//////////////////////////////////////////////////////////////////////

// Deterministic
void RunSimulation(size_t seed, bool explore) {
  auto& runner = checkers::TestRunner::Access();

  runner.Report() << "Simulation seed: " << seed << std::endl;

  // Randomize simulation parameters
  const size_t replicas = 3;
  const size_t getters = 1;
  const size_t setters = 1;
  const size_t keys = 1;

  runner.Report() << "Parameters: "
                  << "replicas = " << replicas << ", "
                  << "getters = " << getters << ", "
                  << "setters = " << setters << ", "
                  << "keys = " << keys << std::endl;

  // TODO pass as arg
  std::string main_log("/home/wasd/thesis/checkers/logs/log.txt");
  checkers::World world{seed, main_log};

  // Cluster
  world.SetupServers(replicas, NaiveKVNode);

  // Clients
  world.SetupClients(getters, kv::GetterRoutine);
  world.SetupClients(setters, kv::SetterRoutine);

  // Predicate
  world.SetPredicate(std::make_shared<kv::KVPredicate<kv::Key, kv::Value>>());

  // Globals
  world.SetGlobal("keys", keys);
  world.SetGlobal("requests", 0ul);

  // Explore
  checkers::Checker checker(world);

  //  std::vector<size_t> deliveries = {1, 3, 3, 0, 4, 4, 3, 3, 3,
  //                                    3, 4, 5, 3, 4, 4, 1, 3, 3};
  //  using Step = checkers::Checker::Step;
  //  std::deque<Step> trace;
  //  for (size_t delivery : deliveries) {
  //    trace.push_back(Step{checkers::MessageDelivery{delivery, 0}, 0});
  //  }
  //  checker.RunBranch(trace);
  //  runner.Fail();

  if (explore) {
    auto trace = checker.Explore(/*steps=*/-1);

    if (!trace.empty()) {
      checker.RunBranch(trace);

      runner.Fail();
    }
  } else {
    checker.RunFirstBranch();
  }
}

int main(int argc, const char** argv) {
  return checkers::Main(argc, argv, RunSimulation);
}
