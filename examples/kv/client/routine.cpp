#include <client/routine.hpp>
#include <client/client.hpp>
#include <client/predicate.hpp>

// Node
#include <whirl/node/runtime/shortcuts.hpp>

// Logging
#include <timber/log.hpp>

// Concurrency
#include <await/fibers/core/api.hpp>

// TODO
#include <checkers/world/world.hpp>

using namespace whirl;

namespace kv {

void GetterRoutine() {
  await::fibers::self::SetName("get");

  timber::Logger logger_{"Client", node::rt::LoggerBackend()};

  auto client =
      ::commute::rpc::MakeClient(node::rt::NetTransport(), node::rt::Executor(),
                                 node::rt::LoggerBackend());

  auto channel =
      client->Dial(fmt::format("{}:{}", /*host=*/"server-1", /*port=*/42));

  BlockingClient kv_store{channel};

  // TODO
  auto predicate = whirl::checkers::ThisWorld()->GetPredicate();

  whirl::checkers::ActorId my_id = node::rt::Config()->GetInt64("node.id");
  using Report = KVReport<Key, Value>;

  Key key = "a";

  LOG_INFO("Execute Get({})", key);
  Value result = kv_store.Get(key);
  LOG_INFO("Get({}) -> {}", key, result);

  Report first_report{
      .actor = my_id, .is_set = false, .key = key, .value = result};
  predicate->Report(muesli::Serialize(first_report));

  LOG_INFO("Execute Get({})", key);
  result = kv_store.Get(key);
  LOG_INFO("Get({}) -> {}", key, result);

  Report second_report{
      .actor = my_id, .is_set = false, .key = key, .value = result};
  predicate->Report(muesli::Serialize(second_report));
}

void SetterRoutine() {
  await::fibers::self::SetName("set");

  timber::Logger logger_{"Client", node::rt::LoggerBackend()};

  auto client =
      ::commute::rpc::MakeClient(node::rt::NetTransport(), node::rt::Executor(),
                                 node::rt::LoggerBackend());

  auto channel =
      client->Dial(fmt::format("{}:{}", /*host=*/"server-2", /*port=*/42));

  BlockingClient kv_store{channel};

  // TODO
  auto predicate = whirl::checkers::ThisWorld()->GetPredicate();

  whirl::checkers::ActorId my_id = node::rt::Config()->GetInt64("node.id");
  using Report = kv::KVReport<Key, Value>;

  Key key = "a";
  Value value = "1";
  LOG_INFO("Execute Set({}, {})", key, value);
  kv_store.Set(key, value);
  LOG_INFO("Set completed");

  Report report{.actor = my_id, .is_set = true, .key = key, .value = value};
  predicate->Report(muesli::Serialize(report));
}

}  // namespace kv
