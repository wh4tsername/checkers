#pragma once

#include <checkers/predicate/predicate.hpp>
#include <checkers/world/actor.hpp>

#include <muesli/serializable.hpp>
#include <muesli/serialize.hpp>

using namespace whirl;

namespace kv {

template <class Key, class Value>
struct KVReport {
  checkers::ActorId actor;

  bool is_set;
  Key key;
  Value value;

  MUESLI_SERIALIZABLE(actor, is_set, key, value)
};

template <class Key, class Value>
class KVPredicate : public checkers::IPredicate {
  using ReportObject = KVReport<Key, Value>;

 public:
  virtual ~KVPredicate() = default;

  void Fail([[maybe_unused]] const std::string& reason) override {
    is_failed_ = true;
  }
  void Report(const std::string& state) override {
    auto report = muesli::Deserialize<ReportObject>(state);

    reports_[free_slot_++] = std::move(report);

    if (IsCorrect() && MakesSense()) {
      CheckReports();
    }
  }

  bool IsCorrect() override {
    return !is_failed_;
  }
  bool MakesSense() override {
    return free_slot_ == 3;
  }

  void Reset() override {
    is_failed_ = false;
    free_slot_ = 0;
  }

 private:
  void CheckReports() {
    ReportObject* first_get = nullptr;
    ReportObject* second_get = nullptr;
    for (ReportObject& report : reports_) {
      if (!report.is_set) {
        if (!first_get) {
          first_get = &report;
        } else {
          second_get = &report;
        }
      }
    }

    is_failed_ = first_get->value == "1" && second_get->value == "";
  }

 private:
  bool is_failed_{false};

  std::array<ReportObject, 3> reports_;
  size_t free_slot_{0};
};

}  // namespace kv
