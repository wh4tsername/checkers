begin_task()

task_link_libraries(whirl-frontend)

# Task libraries
add_task_library(server factorial-server)
add_task_library(client factorial-client)

# Tests
task_link_libraries(checkers)
add_task_test_dir(main/ main)

end_task()
