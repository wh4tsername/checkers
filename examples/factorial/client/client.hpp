#pragma once

// Node
#include <whirl/node/runtime/shortcuts.hpp>
#include <whirl/node/rpc/server.hpp>
#include <whirl/node/cluster/peer.hpp>

// Concurrency
#include <await/fibers/core/api.hpp>
#include <await/fibers/sync/future.hpp>

// RPC
#include <commute/rpc/id.hpp>
#include <commute/rpc/service_base.hpp>
#include <commute/rpc/call.hpp>

// Logging
#include <timber/log.hpp>

namespace factorial {

using await::futures::Future;
using namespace whirl;

class BlockingClient {
 public:
  explicit BlockingClient(commute::rpc::IChannelPtr channel)
      : channel_(channel) {
  }

  void Take(size_t num_requests) {
    std::vector<Future<void>> futures;
    for (size_t i = 0; i < num_requests; ++i) {
      auto future = commute::rpc::Call("Server.Take")
                        .Args(i)
                        .Via(channel_)
                        .TraceWith(GenerateTraceId("Take"))
                        .Start()
                        .As<void>();

      futures.emplace_back(std::move(future));
    }

    for (size_t i = 0; i < num_requests; ++i) {
      await::fibers::Await(std::move(futures[i])).ExpectOk();

      checkers::GlobalCounter("requests").Increment();
    }
  }

 private:
  std::string GenerateTraceId(std::string op) const {
    return fmt::format("{}-{}", op, whirl::node::rt::GenerateGuid());
  }

 private:
  commute::rpc::IChannelPtr channel_;
};

void Client(size_t num_requests) {
  await::fibers::self::SetName("main");

  timber::Logger logger_{"Client", node::rt::LoggerBackend()};

  auto client =
      ::commute::rpc::MakeClient(node::rt::NetTransport(), node::rt::Executor(),
                                 node::rt::LoggerBackend());

  auto channel =
      client->Dial(fmt::format("{}:{}", /*host=*/"server-1", /*port=*/42));

  BlockingClient stub(channel);

  LOG_INFO("Init numbers");
  stub.Take(num_requests);
  LOG_INFO("Done numbers");
}

}  // namespace factorial
