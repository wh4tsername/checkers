#pragma once

#include <stddef.h>
#include <string>

namespace factorial {

void Server(size_t num_requests, const std::string& result_log);

}
