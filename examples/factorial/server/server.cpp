#include <server/server.hpp>

// Node
#include <whirl/node/runtime/shortcuts.hpp>
#include <whirl/node/rpc/server.hpp>
#include <whirl/node/cluster/peer.hpp>
#include <whirl/node/store/kv.hpp>

// Concurrency
#include <await/fibers/core/api.hpp>
#include <await/fibers/sync/mutex.hpp>
#include <await/fibers/sync/future.hpp>
#include <await/futures/util/never.hpp>

// RPC
#include <commute/rpc/id.hpp>
#include <commute/rpc/service_base.hpp>
#include <commute/rpc/call.hpp>

#include <fstream>

namespace factorial {

using await::fibers::Mutex;
using await::futures::Future;
using namespace whirl;

class NumberServer : public commute::rpc::ServiceBase<NumberServer>,
                     public node::cluster::Peer {
 public:
  explicit NumberServer(std::string path, size_t num_requests)
      : Peer(node::rt::Config()),
        path_(path),
        numbers_(node::rt::Database(), "data"),
        request_threshold_(num_requests){
  }

  ~NumberServer() {
  }

  void RegisterMethods() override {
    COMMUTE_RPC_REGISTER_METHOD(Take);
  };

  // RPC handlers

  void Take(size_t number) {
    std::lock_guard guard(lock_);

    numbers_.Put(std::to_string(count_), number);
    ++count_;

    if (count_ == request_threshold_) {
      std::ofstream ofs(path_, std::ios::app);
      for (size_t index = 0; index < count_; ++index) {
        ofs << std::to_string(numbers_.Get(std::to_string(index))) << " ";
      }
      ofs << std::endl;
    }
  }

 private:
  std::string path_;

  Mutex lock_;

  node::store::KVStore<size_t> numbers_;

  size_t count_ = 0;
  size_t request_threshold_;
};

void Server(size_t num_requests, const std::string& result_log) {
  // Start RPC server

  auto rpc_port = node::rt::Config()->GetInt<uint16_t>("rpc.port");
  auto rpc_server = node::rpc::MakeServer(rpc_port);

  rpc_server->RegisterService(
      "Server", std::make_shared<NumberServer>(result_log, num_requests));

  rpc_server->Start();

  // Serving ...

  await::futures::BlockForever();
}

}  // namespace factorial
