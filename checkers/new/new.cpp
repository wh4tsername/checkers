#include <checkers/new/new.hpp>

#include <checkers/new/tracker.hpp>
#include <checkers/new/debug.hpp>

#include <wheels/support/panic.hpp>

#include <cstdlib>
#include <new>

//////////////////////////////////////////////////////////////////////

static thread_local whirl::checkers::IMemoryAllocator* allocator{nullptr};

namespace whirl::checkers {

void SetAllocator(IMemoryAllocator* a) {
  allocator = a;
}

IMemoryAllocator* GetCurrentAllocator() {
  return allocator;
}

}  // namespace whirl::checkers

//////////////////////////////////////////////////////////////////////

static uintptr_t global_allocs_checksum = 0;
static GlobalAllocTracker global_allocs_tracker;

static void* AllocateGlobal(size_t size) {
  if (void* addr = std::malloc(size)) {
    global_allocs_checksum ^= (uintptr_t)addr;
    global_allocs_tracker.Allocate(addr, size);
    return addr;
  } else {
    WHEELS_PANIC("Failed to malloc " << size << " bytes");
  }
}

static void FreeGlobal(void* addr) {
  std::free(addr);
  global_allocs_checksum ^= (uintptr_t)addr;
  global_allocs_tracker.Deallocate(addr);
}

//////////////////////////////////////////////////////////////////////

uintptr_t GlobalAllocsCheckSum() {
  return global_allocs_checksum;
}

void ActivateAllocsTracker() {
  global_allocs_tracker.Activate();
}

void PrintAllocsTrackerReport() {
  global_allocs_tracker.PrintReport();
}

//////////////////////////////////////////////////////////////////////

#if !__has_feature(address_sanitizer)

#include <fstream>

static std::ofstream out("/home/wasd/thesis/checkers/logs/new.txt");
static bool active{false};
static size_t counter{0};

void ActivateLogging() {
  active = true;
}

void DeactivateLogging() {
  active = false;
}

void SplitLogging() {
  out << "-------------------------------------------------------------------"
      << std::endl;
}

void* operator new(size_t size) {
  if (allocator != nullptr) {
    return allocator->Allocate(size);
  }

  auto ptr = AllocateGlobal(size);

  if (active) {
    out << "NEW\t" << ptr << std::endl;
    ++counter;
  }

  return ptr;
}

void operator delete(void* addr) noexcept {
  if (allocator != nullptr) {
    allocator->Free(addr);
    return;
  }

  if (active) {
    out << "DEL\t" << addr << std::endl;
    ++counter;
  }

  FreeGlobal(addr);
}

#endif
