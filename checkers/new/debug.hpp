#pragma once

#include <cstdint>

// For debugging

uintptr_t GlobalAllocsCheckSum();

void ActivateLogging();
void DeactivateLogging();
void SplitLogging();

void ActivateAllocsTracker();
void PrintAllocsTrackerReport();
