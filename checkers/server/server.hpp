#pragma once

#include <whirl/runtime/runtime.hpp>
#include <whirl/node/program/main.hpp>

#include <await/executors/manual.hpp>

#include <checkers/net/router.hpp>
#include <checkers/net/transport.hpp>
#include <checkers/net/network.hpp>
#include <checkers/process/memory.hpp>
#include <checkers/world/actor.hpp>
#include <checkers/config/server.hpp>
#include <checkers/predicate/predicate.hpp>
#include <checkers/db/database.hpp>

namespace whirl::checkers {

class Server : public IActor {
 public:
  Server(Memory& memory, ServerConfig config, node::program::Main program);

  // IActor
  std::string Name() const override {
    return config_.hostname;
  }

  ActorId Id() const override {
    return config_.id;
  }

  void Start() override;

  void Shutdown() override;

  node::IRuntime& GetNodeRuntime();

  std::shared_ptr<IPredicate> GetPredicate() {
    return predicate_;
  }

  void SetPredicate(std::shared_ptr<IPredicate> predicate) {
    predicate_ = std::move(predicate);
  }

  // Context: Network
  std::vector<ActorMessage> HandleMessage(
      std::optional<ActorMessage> msg) override;

  void Register(IActor& peer) override;

  size_t Hash();

 private:
  // Context: Server
  void Step();

  node::IRuntime* MakeNodeRuntime();
  void DestroyNodeRuntime(node::IRuntime* runtime);

 private:
  ServerConfig config_;
  node::program::Main program_;

  Scheduler scheduler_;
  Memory& memory_;
  db::Database* database_;

  Router router_;
  Transport* transport_;

  node::IRuntime* runtime_{nullptr};

  std::shared_ptr<IPredicate> predicate_;
};

////////////////////////////////////////////////////////////////////////////////

Server& ThisServer();

}  // namespace whirl::checkers
