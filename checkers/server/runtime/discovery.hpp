#pragma once

#include <whirl/node/cluster/discovery.hpp>

#include <checkers/world/global/global.hpp>

namespace whirl::checkers {

class DiscoveryService : public node::cluster::IDiscoveryService {
 public:
  node::cluster::List ListPool(const std::string& name) override {
    return GetPool(name);
  }
};

}  // namespace whirl::checkers
