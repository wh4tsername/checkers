#pragma once

#include <whirl/node/guids/service.hpp>

#include <checkers/world/global/guid.hpp>

namespace whirl::checkers {

struct GuidGenerator : public node::guids::IGuidGenerator {
  GuidGenerator() = default;

  std::string Generate() override {
    return GenerateGuid();
  }
};

}  // namespace whirl::checkers
