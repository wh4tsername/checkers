#pragma once

#include <whirl/runtime/runtime.hpp>

// Runtime impl

#include <checkers/server/runtime/random.hpp>
#include <await/executors/manual.hpp>
#include <checkers/process/fibers.hpp>
#include <checkers/server/runtime/time.hpp>
#include <checkers/server/runtime/guid.hpp>
#include <checkers/net/transport.hpp>
#include <checkers/db/database.hpp>
#include <checkers/server/runtime/discovery.hpp>
#include <checkers/config/config.hpp>
#include <checkers/world/global/log.hpp>

#include <optional>

////////////////////////////////////////////////////////////////////////////////

namespace whirl::checkers {

template <typename T>
class StaticObject {
 public:
  void Init(T* obj) {
    object_ = obj;
  }

  T* Get() {
    return *object_;
  }

  T* operator->() {
    return Get();
  }

 private:
  std::optional<T*> object_;
};

////////////////////////////////////////////////////////////////////////////////

struct NodeRuntime : node::IRuntime {
  // IRuntime
  StaticObject<await::executors::ManualExecutor> executor;
  StaticObject<checkers::FiberManager> fibers;
  StaticObject<checkers::Transport> transport;
  StaticObject<checkers::cfg::NodeConfig> config;
  StaticObject<checkers::DiscoveryService> discovery;
  StaticObject<checkers::RandomGenerator> random;
  StaticObject<checkers::GuidGenerator> guids;
  StaticObject<checkers::db::Database> database;
  //  StaticObject<checkers::TimeService> time;

  await::executors::IExecutor* Executor() override {
    return executor.Get();
  }

  await::fibers::IFiberManager* FiberManager() override {
    return fibers.Get();
  }

  node::time::ITimeService* TimeService() override {
    return nullptr;
  }

  commute::transport::ITransport* NetTransport() override {
    return transport.Get();
  }

  node::db::IDatabase* Database() override {
    return database.Get();
  }

  node::guids::IGuidGenerator* GuidGenerator() override {
    return guids.Get();
  }

  node::random::IRandomService* RandomService() override {
    return random.Get();
  }

  node::time::ITrueTimeService* TrueTime() override {
    return nullptr;
  }

  persist::fs::IFileSystem* FileSystem() override {
    return nullptr;
  }

  timber::ILogBackend* LoggerBackend() override {
    return GetLogBackend();
  }

  node::cfg::IConfig* Config() override {
    return config.Get();
  }

  node::cluster::IDiscoveryService* DiscoveryService() override {
    return discovery.Get();
  }

  node::ITerminal* Terminal() override {
    return nullptr;
  }
};

}  // namespace whirl::checkers
