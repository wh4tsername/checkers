#include <checkers/server/server.hpp>
#include <checkers/server/runtime/runtime.hpp>
#include <checkers/process/schedule.hpp>
#include <checkers/process/trampoline.hpp>
#include <checkers/world/global/actor.hpp>
#include <checkers/world/global/scope.hpp>
#include <checkers/net/transport_message.hpp>

#include <muesli/serialize.hpp>
#include <wheels/support/hash.hpp>

namespace whirl::checkers {

////////////////////////////////////////////////////////////////////////////////

Server::Server(Memory& memory, ServerConfig config, node::program::Main program)
    : config_(config),
      program_(program),
      memory_(memory) {
}

void Server::Start() {
  auto g = memory_.Use();

  runtime_ = MakeNodeRuntime();

  Schedule(scheduler_, [this]() {
    auto g = memory_.Use();

    MainTrampoline(program_);
  });
}

void Server::Step() {
  auto g = memory_.Use();

  scheduler_.Drain();
}

void Server::Shutdown() {
  // TODO check for allocator scopes
  auto g = memory_.Use();

  DestroyNodeRuntime(runtime_);

  runtime_ = nullptr;
}

node::IRuntime& Server::GetNodeRuntime() {
  return *runtime_;
}

std::vector<ActorMessage> Server::HandleMessage(
    std::optional<ActorMessage> message) {
  auto actor_scope = SwitchToActor(this);

  {
    // Step 1: Global heap -> Local heap

    auto g = memory_.Use();

    if (message) {
      // NB: clear previous messages under server heap

      // Deserialize and copy to server heap
      auto transport_message =
          muesli::Deserialize<TransportMessage>(message->data);
      transport_->HandleMessage(std::move(transport_message));
    }

    Step();
  }

  {
    // Step 2: Local heap -> Global heap

    std::vector<ActorMessage> out_messages;

    // Copy to global heap
    for (const auto& out : transport_->OutMessages()) {
      out_messages.push_back(out);  // Copy
    }

    return out_messages;
  }
}

void Server::Register(IActor& peer) {
  router_.Register(peer);
}

// Private

node::IRuntime* Server::MakeNodeRuntime() {
  NodeRuntime* runtime = new NodeRuntime;

  runtime->executor.Init(&scheduler_);
  runtime->fibers.Init(new FiberManager);

  transport_ = new Transport(router_, config_, scheduler_);
  runtime->transport.Init(transport_);

  runtime->config.Init(new cfg::NodeConfig(config_));

  runtime->discovery.Init(new DiscoveryService);

  runtime->random.Init(new RandomGenerator);

  runtime->guids.Init(new GuidGenerator);

  database_ = new db::Database(GetLogBackend());
  runtime->database.Init(database_);

  return runtime;
}

void Server::DestroyNodeRuntime(node::IRuntime* runtime) {
  delete runtime->FiberManager();

  delete runtime->NetTransport();

  delete runtime->Config();

  delete runtime->DiscoveryService();

  delete runtime->RandomService();

  delete runtime->GuidGenerator();

  delete runtime->Database();

  delete runtime;
}

size_t Server::Hash() {
  auto actor_scope = SwitchToActor(this);
  
  size_t hash = 0;
#ifndef NO_HEAP
  wheels::HashCombine(hash, memory_.Hash());
#endif
  wheels::HashCombine(hash, database_->Hash());

  return hash;
}

////////////////////////////////////////////////////////////////////////////////

Server& ThisServer() {
  Server* this_server = dynamic_cast<Server*>(ThisActor());
  WHEELS_VERIFY(this_server != nullptr, "Current actor is not a server");
  return *this_server;
}

}  // namespace whirl::checkers
