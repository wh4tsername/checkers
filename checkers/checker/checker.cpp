#include <checkers/checker/checker.hpp>

#ifdef INSTANT_RESPONSE
#include <commute/rpc/wire.hpp>
#endif

namespace whirl::checkers {

bool Checker::ExecChain(size_t max_depth) {
  if (chain_count_ % 1000 == 0) {
    log_ << "\nPath #" << chain_count_;

#ifdef HASHING
    log_ << "\nStates visited " << visited_.size();
#endif

    log_ << std::endl;
  }

  if (buffer_.empty()) {
    size_t num_messages = network_.CountMessages();

    for (size_t message_index = 0; message_index < num_messages;
         ++message_index) {
      buffer_.push({{message_index, num_messages}, 1});
    }

    start_state_processed_ = true;
  } else {
    auto [cur_node, cur_depth] = buffer_.top();
    while (!path_.empty() && path_.size() != cur_depth - 1) {
      path_.pop_back();
    }

    for (auto [node, node_depth] : path_) {
      auto hash = MakeDelivery(node);
      if (!hash) {
        return false;
      }
    }
  }

  size_t prev_depth = 0;
  while (!buffer_.empty()) {
    auto [cur_node, cur_depth] = buffer_.top();

    if (cur_depth != 0 && cur_depth <= prev_depth) {
      break;
    }

    buffer_.pop();

    auto hash = MakeDelivery(cur_node);
    path_.emplace_back(Step{cur_node, cur_depth});

    if (!hash) {
      return false;
    }

#ifdef HASHING
    if (visited_.contains(*hash)) {
      return FinalizeBranch();
    } else {
      visited_.insert(*hash);
    }
#endif

    if (cur_depth >= max_depth) {
      break;
    }

    size_t num_messages = network_.CountMessages();
    for (size_t i = 0; i < num_messages; ++i) {
      buffer_.push({{i, num_messages}, cur_depth + 1});
    }

    prev_depth = cur_depth;
  }

  return FinalizeBranch();
}

bool Checker::FinalizeBranch() {
  if (buffer_.empty() && start_state_processed_) {
    log_ << (chain_count_ % 1000 != 0
                 ? "\nPath #" + std::to_string(chain_count_) + "\n"
                 : "")
         << "DFS finished!" << std::endl;
    return true;
  } else {
    ++chain_count_;

    return false;
  }
}

std::optional<size_t> Checker::MakeDelivery(const MessageDelivery& delivery) {
  LOG_INFO("Message index: {}", delivery.message_index_);

  ActorMessage message = network_.ExtractMessage(delivery.message_index_);

  size_t hash = world_.Step(std::move(message));

#ifdef INSTANT_RESPONSE
  return InstantResponseDelivery(hash);
#endif

  if (predicate_ && !predicate_->IsCorrect()) {
    return {};
  }

  return hash;
}

#ifdef INSTANT_RESPONSE
std::optional<size_t> Checker::InstantResponseDelivery(size_t hash) {
  for (size_t message_index = 0; message_index < network_.CountMessages();
       ++message_index) {
    const ActorMessage& actor_message = network_.LookUpMessage(message_index);

    auto transport_message =
        muesli::Deserialize<TransportMessage>(actor_message.data);

    try {
      auto rpc_response = muesli::Deserialize<commute::rpc::proto::Response>(
          transport_message.payload);

      {
        ActorMessage message = network_.ExtractMessage(message_index);

        hash = world_.Step(std::move(message));
        if (predicate_ && !predicate_->IsCorrect()) {
          return {};
        }
      }

      // TODO start again when all prev responses delivered
      message_index = 0;
    } catch (...) {
    }
  }

  return hash;
}
#endif

}  // namespace whirl::checkers
