#pragma once

#include <stack>
#include <unordered_set>

#include <wheels/support/assert.hpp>

#include <checkers/checker/message_delivery.hpp>
#include <checkers/net/network.hpp>
#include <checkers/world/actor.hpp>
#include <checkers/world/actor_ctx.hpp>
#include <checkers/world/world.hpp>
#include <checkers/new/debug.hpp>

#include <commute/rpc/id.hpp>

namespace whirl::checkers {

class Checker {
 public:
  struct Step {
    MessageDelivery delivery;
    size_t depth;
  };

  explicit Checker(World& world)
      : log_(std::cout),
        logger_("Checker", &world.GetLog()),
        world_(world),
        network_(world.GetNetwork()) {
  }

  size_t RunFirstBranch() {
    World::WorldGuard g(&world_);

    world_.SetLogging();

    LOG_INFO("Determinism test run");

    // Reset RPC ids
    commute::rpc::ResetIds();

    world_.Start();

    world_.Step({});

    predicate_ = world_.GetPredicate();

    ExecChain(-1);

    world_.Shutdown();

    // Reset predicate
    if (predicate_) {
      predicate_->Reset();
    }

    Reset();

    return GlobalAllocsCheckSum();
  }

  std::deque<Step> Explore(size_t max_steps) {
    bool dfs_finished = false;
    while (!dfs_finished) {
      World::WorldGuard g(&world_);

      // Reset RPC ids
      commute::rpc::ResetIds();

      world_.Start();

      world_.Step({});

      predicate_ = world_.GetPredicate();

      dfs_finished = ExecChain(max_steps);

      world_.Shutdown();

      if (predicate_ && predicate_->MakesSense() && !predicate_->IsCorrect()) {
        predicate_->Reset();

        return path_;
      }

      // Reset predicate
      if (predicate_) {
        predicate_->Reset();
      }
    }

    return {};
  }

  void RunBranch(const std::deque<Step>& trace) {
    World::WorldGuard g(&world_);

    world_.SetLogging();

    // Reset RPC ids
    commute::rpc::ResetIds();

    world_.Start();

    world_.Step({});

    predicate_ = world_.GetPredicate();
    if (!predicate_) {
      WHEELS_UNREACHABLE();
    }

    for (const Step& step : trace) {
      auto hash = MakeDelivery(step.delivery);

      if (!hash) {
        break;
      }
    }

    world_.Shutdown();

    if (!predicate_->MakesSense() || predicate_->IsCorrect()) {
      WHEELS_UNREACHABLE();
    }

    // Reset predicate
    predicate_->Reset();
  }

 private:
  bool ExecChain(size_t max_depth);

  [[nodiscard]] std::optional<size_t> MakeDelivery(
      const MessageDelivery& delivery);

  bool FinalizeBranch();

  void Reset() {
    path_.clear();

    while (!buffer_.empty()) {
      buffer_.pop();
    }

    chain_count_ = 1;
    start_state_processed_ = false;

    predicate_ = nullptr;

#ifdef HASHING
    visited_.clear();
#endif
  }

#ifdef INSTANT_RESPONSE
  std::optional<size_t> InstantResponseDelivery(size_t hash);
#endif

  std::ostream& log_;
  timber::Logger logger_;

  World& world_;
  Network& network_;

  std::deque<Step> path_;
  std::stack<std::pair<MessageDelivery, size_t>> buffer_;
  size_t chain_count_ = 1;
  bool start_state_processed_ = false;

  std::shared_ptr<IPredicate> predicate_;

#ifdef HASHING
  std::unordered_set<size_t> visited_;
#endif
};

}  // namespace whirl::checkers
