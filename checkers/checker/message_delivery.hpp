#pragma once

#include <checkers/net/network.hpp>

namespace whirl::checkers {

struct MessageDelivery {
  size_t message_index_ = 0;
  size_t num_children_ = 0;
};

}  // namespace whirl::checkers
