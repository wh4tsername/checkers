#pragma once

#include <wheels/memory/mmap_allocation.hpp>

#include <utility>

namespace whirl::checkers {

//////////////////////////////////////////////////////////////////////

// Set before first simulation
void SetHeapSize(size_t bytes);

wheels::MmapAllocation AcquireHeap();
void ReleaseHeap(wheels::MmapAllocation heap);

}  // namespace whirl::checkers
