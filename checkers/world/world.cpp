#include <checkers/world/world.hpp>
#include <checkers/runtime/setup.hpp>

#include <timber/log.hpp>

#include <iostream>

#include <wheels/support/hash.hpp>

namespace whirl::checkers {

////////////////////////////////////////////////////////////////////////////////

static World* this_world = nullptr;

World::WorldGuard::WorldGuard(World* world) {
  this_world = world;
}

World::WorldGuard::~WorldGuard() {
  this_world = nullptr;
}

World* ThisWorld() {
  if (this_world == nullptr) {
    WHEELS_PANIC("not in world context");
  }

  return this_world;
}

////////////////////////////////////////////////////////////////////////////////

void World::Start() {
  {
    // Cluster
    for (const auto& [servers, server_main] : server_setup_) {
      MakePool(servers, server_main);
    }

    // Clients
    for (const auto& [clients, client_main] : client_setup_) {
      AddClients(clients, client_main);
    }

    // Registration
    for (IActor* actor : actors_) {
      for (IActor* peer : actors_) {
        actor->Register(*peer);
      }
    }

    // Setting predicate
    if (predicate_) {
      for (auto& server : servers_) {
        server.SetPredicate(predicate_);
      }
    }
  }

  SetupCheckersRuntime();

  time_.SetStartTime();

  size_t num_servers = 0;
  for (const auto& [servers, _] : server_setup_) {
    num_servers += servers;
  }

  size_t num_clients = 0;
  for (const auto& [clients, _] : client_setup_) {
    num_clients += clients;
  }

  LOG_INFO("Cluster: {}, clients: {}", num_servers, num_clients);

  LOG_INFO("Starting actors...");

  for (auto& actor : actors_) {
    Scope(actor)->Start();
  }

  LOG_INFO("World started");
}

size_t World::Step(std::optional<ActorMessage> message) {
  if (!message) {
    // Reactive initialization

    for (auto& actor : actors_) {
      auto out = Scope(actor)->HandleMessage({});

      network_.AddMessages(out);
    }
  } else {
    // Message delivery

    //  std::cout << network_.CountMessages() << std::endl;

    auto out_msgs =
        actors_[message->dest - 1]->HandleMessage(std::move(message));
    network_.AddMessages(out_msgs);
  }

  size_t digest = 0;
  wheels::HashCombine(digest, network_.Hash());

  for (Server& server : servers_) {
    wheels::HashCombine(digest, server.Hash());
  }

  return digest;
}

void World::Shutdown() {
  for (auto& server : servers_) {
    Scope(server)->Shutdown();
  }
  servers_.clear();

  actors_.clear();

  pools_.clear();

  network_.Reset();

  shared_memory_.Reset();

  server_ids_.Reset();
  guids_.Reset();

  random_source_.Reset(seed_);

  LOG_INFO("World stopped");
}

}  // namespace whirl::checkers
