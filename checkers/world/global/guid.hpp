#pragma once

#include <string>

namespace whirl::checkers {

std::string GenerateGuid();

}  // namespace whirl::checkers
