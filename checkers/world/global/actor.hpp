#pragma once

#include <checkers/world/actor.hpp>

namespace whirl::checkers {

bool AmIActor();
IActor* ThisActor();

}  // namespace whirl::checkers
