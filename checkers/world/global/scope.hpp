#pragma once

#include <checkers/world/actor_ctx.hpp>

namespace whirl::checkers {

ActorContext::ScopeGuard SwitchToActor(IActor* actor);

}  // namespace whirl::checkers
