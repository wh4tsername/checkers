#pragma once

#include <timber/backend.hpp>

namespace whirl::checkers {

timber::ILogBackend* GetLogBackend();

}  // namespace whirl::checkers
