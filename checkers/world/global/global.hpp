#include <checkers/world/global/time.hpp>
#include <checkers/world/global/actor.hpp>

#include <cstdlib>
#include <vector>
#include <string>
#include <any>

namespace whirl::checkers {

// Global world services used by different components of simulation

//////////////////////////////////////////////////////////////////////

size_t WorldSeed();

std::vector<std::string> GetPool(const std::string& name);

}  // namespace whirl::checkers
