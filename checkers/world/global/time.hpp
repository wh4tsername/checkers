#pragma once

#include <checkers/time/time_point.hpp>

namespace whirl::checkers {

TimePoint GlobalNow();

}
