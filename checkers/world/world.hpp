#pragma once

#include <checkers/world/random_source.hpp>
#include <checkers/time/time.hpp>
#include <checkers/helpers/untyped_dict.hpp>
#include <checkers/process/memory.hpp>
#include <checkers/world/actor.hpp>
#include <checkers/world/actor_ctx.hpp>
#include <checkers/server/server.hpp>
#include <checkers/log/backend.hpp>

#include <vector>
#include <queue>

namespace whirl::checkers {

class World {
 public:
  struct WorldGuard {
    explicit WorldGuard(World* world);
    ~WorldGuard();
  };

  using Servers = std::deque<Server>;

  explicit World(size_t seed, const std::string& path)
      : seed_(seed),
        random_source_(seed),
        path_(path),
        logger_("World", &log_backend_) {
  }

  void SetLogging() {
    log_backend_.AppendToFile(path_);
  }

  void Start();

  size_t Step(std::optional<ActorMessage> message);

  void Shutdown();

  // Access

  Network& GetNetwork() {
    return network_;
  }

  size_t Seed() {
    return seed_;
  }

  void SetGlobal(const std::string& name, std::any value) {
    globals_.Set(name, value);
  }

  std::any GetGlobal(const std::string& name) const {
    return globals_.Get(name);
  }

  uint64_t RandomNumber() {
    return random_source_.Next();
  }

  std::string GenerateGuid() {
    return std::to_string(guids_.NextId());
  }

  log::LogBackend& GetLog() {
    return log_backend_;
  }

  TimePoint Now() {
    return time_.Now();
  }

  IActor* CurrentActor() const {
    return active_.Get();
  }

  ActorContext::ScopeGuard Scope(IActor* actor) {
    return active_.Scope(actor);
  }

  ActorContext::ScopeGuard Scope(IActor& actor) {
    return active_.Scope(&actor);
  }

  // Servers

  void SetupServers(size_t count, node::program::Main program) {
    server_setup_.emplace_back(std::make_pair(count, program));
  }

  void SetupClients(size_t count, node::program::Main program) {
    client_setup_.emplace_back(std::make_pair(count, program));
  }

  std::shared_ptr<IPredicate> GetPredicate() {
    return predicate_;
  }

  void SetPredicate(std::shared_ptr<IPredicate> predicate) {
    predicate_ = std::move(predicate);
  }

  std::vector<std::string> GetPool(const std::string& name) {
    return pools_[name];
  }

 private:
  // Servers

  void MakePool(size_t count, node::program::Main program) {
    pools_["servers"] = {};
    for (size_t i = 0; i < count; ++i) {
      AddActor(servers_, "server", program, "servers");
    }
  }

  void AddClients(size_t count, node::program::Main program) {
    pools_["clients"] = {};
    for (size_t i = 0; i < count; ++i) {
      AddActor(servers_, "client", program, "clients");
    }
  }

  void AddActor(Servers& group, std::string name, node::program::Main program,
                std::string pool) {
    auto id = server_ids_.NextId();

    auto server_name = MakeServerName(std::move(name), id);

    pools_[pool].push_back(server_name);

    group.emplace_back(shared_memory_,
                       ServerConfig{id, std::move(server_name), pool}, program);

    actors_.emplace_back(&group.back());
  }

  std::string MakeServerName(std::string name_template, size_t index) {
    wheels::StringBuilder name;
    name << name_template << '-' << index;
    return name;
  }

 private:
  size_t seed_;

  // Setup
  std::vector<std::pair<size_t, node::program::Main>> server_setup_;
  std::vector<std::pair<size_t, node::program::Main>> client_setup_;

  log::LogBackend log_backend_;

  ActorContext active_;

  Time time_;
  RandomSource random_source_;

  UntypedDict globals_;

  Servers servers_;
  std::vector<IActor*> actors_;
  std::unordered_map<std::string, std::vector<std::string>> pools_;

  wheels::IdGenerator guids_;
  wheels::IdGenerator server_ids_;

  Network network_;
  Memory shared_memory_;

  std::string path_;
  timber::Logger logger_;

  std::shared_ptr<IPredicate> predicate_;
};

////////////////////////////////////////////////////////////////////////////////

World* ThisWorld();

}  // namespace whirl::checkers
