#pragma once

#include <checkers/net/packet.hpp>
#include <checkers/net/actor_message.hpp>

#include <string>

namespace whirl::checkers {

struct IActor {
  virtual ~IActor() = default;

  virtual std::string Name() const = 0;
  virtual ActorId Id() const = 0;

  virtual void Start() = 0;

  virtual std::vector<ActorMessage> HandleMessage(
      std::optional<ActorMessage> msg) = 0;

  virtual void Shutdown() = 0;

  virtual void Register(IActor& peer) = 0;
};

}  // namespace whirl::checkers
