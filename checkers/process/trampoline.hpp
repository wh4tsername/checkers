#pragma once

#include <whirl/node/program/main.hpp>

namespace whirl::checkers {

void MainTrampoline(node::program::Main main);

}  // namespace whirl::checkers
