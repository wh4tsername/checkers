#include <checkers/process/trampoline.hpp>

#include <whirl/node/runtime/shortcuts.hpp>

#include <await/fibers/core/fiber.hpp>
#include <await/fibers/static/services.hpp>

namespace whirl::checkers {

void MainTrampoline(node::program::Main main) {
  auto main_fiber = [main]() {
    main();
  };

  auto* f = await::fibers::CreateFiber(
      main_fiber, node::rt::FiberManager(), node::rt::Executor(),
      await::fibers::BackgroundSupervisor(), await::context::NeverStop());

  f->Schedule();
}

}  // namespace whirl::checkers
