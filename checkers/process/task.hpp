#pragma once

#include <await/executors/task.hpp>

namespace whirl::checkers::detail {

template <class F>
class TaskWrapper : public await::executors::TaskBase {
 public:
  explicit TaskWrapper(F&& f) : f_(f) {
  }

  void Run() noexcept override {
    f_();

    delete this;
  }

  void Discard() noexcept override {
  }

 private:
  F f_;
};

}  // namespace whirl::checkers::detail
