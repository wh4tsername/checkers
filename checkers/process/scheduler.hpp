#pragma once

#include <await/executors/manual.hpp>

namespace whirl::checkers {

using Scheduler = await::executors::ManualExecutor;

}  // namespace whirl::checkers
