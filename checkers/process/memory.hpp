#pragma once

#include <checkers/memory/allocator.hpp>
#include <checkers/new/new.hpp>

namespace whirl::checkers {

class Memory {
 public:
  AllocatorGuard Use() {
    return AllocatorGuard(&impl_);
  }

  size_t BytesAllocated() const {
    return impl_.BytesAllocated();
  }

  bool FromHere(void* addr) const {
    return impl_.FromHere(addr);
  }

  void Reset() {
    impl_.Reset();
  }

  size_t Hash() const {
    return impl_.Hash();
  }

 private:
  MemoryAllocator impl_;
};

}  // namespace whirl::checkers
