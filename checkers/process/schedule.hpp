#pragma once

#include <checkers/process/memory.hpp>
#include <checkers/process/task.hpp>

namespace whirl::checkers {

template <class Executor, typename F>
void Schedule(Executor& executor, F&& f) {
  await::executors::TaskBase* task = new detail::TaskWrapper(std::move(f));
  executor.Execute(task);
}

// template <class Executor, typename F>
// void Schedule(Executor& executor, TimePoint t, F&& f) {
//}

}  // namespace whirl::checkers
