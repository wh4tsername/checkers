#pragma once

#include <whirl/node/time/jiffies.hpp>

#include <cstdint>

namespace whirl::checkers {

using TimePoint = uint64_t;

}  // namespace whirl::checkers
