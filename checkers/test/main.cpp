#include <checkers/test/main.hpp>
#include <checkers/test/runner.hpp>

#include <wheels/cmdline/argparse.hpp>

#include <iostream>

namespace whirl::checkers {

//////////////////////////////////////////////////////////////////////

static void CLI(wheels::ArgumentParser& parser) {
  parser.AddHelpFlag();

  parser.Add("det").Flag().Help("Test determinism");
  parser.Add("seed").ValueDescr("uint").Optional();
}

template <typename T>
static T FromString(std::string str) {
  std::istringstream input{str};
  T value;
  input >> value;
  return value;
}

//////////////////////////////////////////////////////////////////////

int Main(int argc, const char** argv, Simulation sim) {
  wheels::ArgumentParser parser{"Model checker"};
  CLI(parser);

  wheels::ParsedArgs args = parser.Parse(argc, argv);

  TestRunner runner{sim};

  size_t seed = args.Has("seed") ? FromString<size_t>(args.Get("seed")) : 42ul;

  if (args.HasFlag("det")) {
    return runner.TestDeterminism(seed);
  }

  runner.RunExploration(seed);

  runner.Congratulate();

  return 0;
}

}  // namespace whirl::checkers
