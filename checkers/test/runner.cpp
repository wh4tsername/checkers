#include <checkers/test/runner.hpp>

#include <checkers/new/debug.hpp>

#include <wheels/support/assert.hpp>

#include <fmt/core.h>

#include <fstream>

namespace fs = std::filesystem;

namespace whirl::checkers {

static thread_local TestRunner* active{nullptr};

TestRunner& TestRunner::Access() {
  WHEELS_ASSERT(active != nullptr, "Not in simulation context");
  return *active;
}

int TestRunner::TestDeterminism(size_t seed) {
#if __has_feature(address_sanitizer)
  std::cerr << "--det is incompatible with Address Sanitizer" << std::endl;
  std::exit(1);
#endif

  ActivateAllocsTracker();

  ActivateLogging();

  active = this;
  sim_(seed, false);
  size_t first_checksum = GlobalAllocsCheckSum();
  active = nullptr;

  PrintAllocsTrackerReport();

  SplitLogging();
  Report() << std::endl;

  active = this;
  sim_(seed, false);
  size_t second_checksum = GlobalAllocsCheckSum();
  active = nullptr;

  PrintAllocsTrackerReport();

  DeactivateLogging();

  if (first_checksum != second_checksum) {
    Report() << std::endl << "Non determinism!" << std::endl;
    Report() << first_checksum << std::endl;
    Report() << second_checksum << std::endl;

    return 1;
  }

  return 0;
}

void TestRunner::RunExploration(size_t seed) {
  Report() << "Run exploration..." << std::endl;

  active = this;
  sim_(seed, true);
  active = nullptr;
}

void TestRunner::Congratulate() {
  std::cout << std::endl << "Looks good! ヽ(‘ー`)ノ" << std::endl;
}

void TestRunner::Fail() {
  std::cout << "(ﾉಥ益ಥ）ﾉ ┻━┻" << std::endl;
  std::cout.flush();
  std::exit(1);
}

}  // namespace whirl::checkers
