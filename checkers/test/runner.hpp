#pragma once

#include <string>
#include <optional>
#include <iostream>
#include <sstream>
#include <filesystem>

#include <checkers/test/simulation.hpp>

namespace whirl::checkers {

class World;

class TestRunner {
 public:
  explicit TestRunner(Simulation sim) : sim_(sim), output_(std::cout) {
  }

  int TestDeterminism(size_t seed);

  void RunExploration(size_t seed);

  // Access current test runner
  static TestRunner& Access();

  // Output streams

  std::ostream& Report() {
    return output_;
  }

  // Fail

  void Fail();

  void Fail(std::string reason) {
    output_ << reason << std::endl;
    Fail();
  }

  void Congratulate();

 private:
  Simulation sim_;

  std::ostream& output_;
};

}  // namespace whirl::checkers
