#pragma once

#include <cstddef>

namespace whirl::checkers {

using Simulation = void (*)(size_t, bool);

}
