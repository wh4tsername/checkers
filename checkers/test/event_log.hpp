#pragma once

#include <checkers/log/event.hpp>

#include <iostream>

namespace whirl::checkers {

void WriteTextLog(const log::EventLog& log, std::ostream& out);

}  // namespace whirl::checkers
