#pragma once

#include <checkers/test/simulation.hpp>

namespace whirl::checkers {

int Main(int argc, const char** argv, Simulation sim);

}  // namespace whirl::checkers
