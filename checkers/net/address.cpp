#include <checkers/net/address.hpp>

namespace whirl::checkers {

Port ParsePort(const std::string& port_str) {
  return std::stoul(port_str);
}

Address ParseAddress(const std::string& address) {
  auto pos = address.find(':');

  auto lhs = address.substr(0, pos);
  auto rhs = address.substr(pos + 1, address.length());

  return Address{lhs, ParsePort(rhs)};
}

}  // namespace whirl::checkers
