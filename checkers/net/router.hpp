#pragma once

#include <checkers/config/server.hpp>
#include <checkers/world/actor.hpp>

namespace whirl::checkers {

class Router {
 public:
  using NetAddress = ActorId;

  void Register(IActor& peer) {
    routing_table_[peer.Name()] = peer.Id();
  }

  NetAddress Resolve(const std::string& host) {
    return routing_table_[host];
  }

 private:
  std::unordered_map<std::string, NetAddress> routing_table_;
};

}  // namespace whirl::checkers
