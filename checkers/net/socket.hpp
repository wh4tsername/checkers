#pragma once

#include <commute/transport/transport.hpp>
#include <checkers/net/address.hpp>
#include <checkers/net/network.hpp>

namespace whirl::checkers {

struct Transport;

struct Socket : public ISocket {
  explicit Socket(Hostname self_host, Port self, Hostname host, Port port,
                  Transport* transport);
  ~Socket() = default;

  const std::string& Peer() const override {
    return host_;
  }
  void Send(const Message& message) override;

  void Close() override {
  }

  bool IsConnected() const override {
    return true;
  }

 private:
  std::string self_port_;
  std::string self_host_;

  std::string port_;
  std::string host_;

  Transport* transport_;
};

}  // namespace whirl::checkers
