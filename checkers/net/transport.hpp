#pragma once

#include <checkers/process/schedule.hpp>
#include <checkers/process/scheduler.hpp>
#include <checkers/net/socket.hpp>
#include <checkers/world/global/actor.hpp>
#include <checkers/world/global/scope.hpp>
#include <checkers/config/config.hpp>
#include <checkers/net/transport_message.hpp>

#include <muesli/serialize.hpp>

namespace whirl::checkers {

struct Transport : public ITransport {
  using Endpoints = std::unordered_map<Port, IHandlerPtr>;

  struct NetServer : public IServer {
    void Shutdown() override {
    }
  };

  explicit Transport(Router& router, const ServerConfig& config,
                     Scheduler& scheduler)
      : router_(router), config_(config), scheduler_(scheduler) {
  }

  virtual ~Transport() = default;

  const std::string& HostName() const override {
    return config_.hostname;
  }

  IServerPtr Serve(const std::string& port, IHandlerPtr handler) override {
    // TODO what if overwrites
    Port self = ParsePort(port);

    endpoints_[self] = handler;

    return std::make_shared<NetServer>();
  }

  ISocketPtr ConnectTo(const std::string& address,
                       IHandlerPtr handler) override {
    Address addr = ParseAddress(address);

    // TODO what if overwrites
    Port self = free_port_++;

    endpoints_[self] = handler;

    auto socket = std::make_shared<Socket>(config_.hostname, self, addr.host,
                                           addr.port, this);

    return socket;
  }

  void AddMessage(const TransportMessage& message) {
    ActorMessage actor_message;
    actor_message.source = config_.id;
    actor_message.dest = router_.Resolve(message.header.host);
    actor_message.data = muesli::Serialize(message);

    actor_message.ComputeHash();

    out_messages_.emplace_back(std::move(actor_message));
  }

  // Context: Server
  void HandleMessage(TransportMessage&& message) {
    out_messages_.clear();

    Port self_port = std::stoul(message.header.self_port);
    Port port = std::stoul(message.header.port);

    auto back = std::make_shared<Socket>(
        message.header.host, port, message.header.self_host, self_port, this);

    auto callback = [message = std::move(message.payload),
                     handler = endpoints_[port], back = std::move(back)]() {
      if (!handler.expired()) {
        handler.lock()->HandleMessage(message, back);
      }
    };

    Schedule(scheduler_, callback);
  }

  const std::vector<ActorMessage>& OutMessages() const {
    return out_messages_;
  }

 private:
  Router& router_;
  const ServerConfig& config_;
  Scheduler& scheduler_;

  std::vector<ActorMessage> out_messages_;

  Endpoints endpoints_;
  Port free_port_{1};
};

}  // namespace whirl::checkers
