#pragma once

#include <checkers/config/config.hpp>

#include <wheels/support/hash.hpp>
#include <checkers/helpers/fast_hash.hpp>

namespace whirl::checkers {

struct ActorMessage {
  void ComputeHash() {
    wheels::HashCombine(hash, FastHash<std::string>{}(data));
    wheels::HashCombine(hash, source);
    wheels::HashCombine(hash, dest);
  }
  
  ActorId dest;
  ActorId source;
  std::string data;

  size_t hash{0};
};

}  // namespace whirl::checkers
