#pragma once

#include <checkers/net/router.hpp>

#include <muesli/serializable.hpp>

namespace whirl::checkers {

struct TransportMessage {
  struct Header {
    std::string self_port;
    std::string self_host;

    std::string port;
    std::string host;

    MUESLI_SERIALIZABLE(self_port, self_host, port, host)
  };

  Header header;
  std::string payload;

  MUESLI_SERIALIZABLE(header, payload)
};

}  // namespace whirl::checkers
