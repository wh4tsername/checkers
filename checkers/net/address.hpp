#pragma once

#include <string>

namespace whirl::checkers {

using Port = uint16_t;
using Hostname = std::string;

struct Address {
  Hostname host;
  Port port;
};

Port ParsePort(const std::string& port_str);

Address ParseAddress(const std::string& address);

}  // namespace whirl::checkers
