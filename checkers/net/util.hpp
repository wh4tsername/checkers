#pragma once

#include <commute/transport/transport.hpp>

namespace whirl::checkers {

class Server;

using ISocket = commute::transport::ISocket;
using ISocketWeakPtr = std::weak_ptr<ISocket>;

}  // namespace whirl::checkers
