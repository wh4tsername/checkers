#include <checkers/net/socket.hpp>
#include <checkers/net/transport.hpp>

namespace whirl::checkers {

Socket::Socket(Hostname self_host, Port self, Hostname host, Port port,
               Transport* transport)
    : self_port_(std::to_string(self)),
      self_host_(std::move(self_host)),
      port_(std::to_string(port)),
      host_(std::move(host)),
      transport_(transport) {
}

void Socket::Send(const Message& message) {
  if (IsConnected()) {
    TransportMessage transport_message{
        .header = TransportMessage::Header{.self_port = self_port_,
                                           .self_host = self_host_,
                                           .port = port_,
                                           .host = host_},
        .payload = message};

    transport_->AddMessage(transport_message);
  }
}

}  // namespace whirl::checkers
