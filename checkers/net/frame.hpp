#pragma once

#include <checkers/net/packet.hpp>
#include <checkers/world/actor.hpp>

#include <boost/functional/hash.hpp>

namespace whirl::checkers {

struct Frame {
  Packet msg;
  IActor* src;
  IActor* dest;
};

// bool operator==(const Frame& lhs, const Frame& rhs) {
//  return lhs.msg == rhs.msg && lhs.src == rhs.src && lhs.dest == rhs.dest;
//}
//
// struct FrameHash {
//  std::size_t operator()(Frame const& frame) const noexcept {
//    std::size_t h1 = MessageHash{}(frame.msg);
//    std::size_t h2 = std::hash<IActor*>{}(frame.src);
//    std::size_t h3 = std::hash<IActor*>{}(frame.dest);
//
//    std::size_t seed = 0;
//    boost::hash_combine(seed, h1);
//    boost::hash_combine(seed, h2);
//    boost::hash_combine(seed, h3);
//
//    return seed;
//  }
//};

}  // namespace whirl::checkers
