#pragma once

#include <commute/transport/transport.hpp>

#include <functional>
#include <vector>
#include <unordered_map>

#include <checkers/net/address.hpp>
#include <checkers/net/packet.hpp>
#include <checkers/net/buffer/buffer.hpp>
#include <checkers/new/new.hpp>
#include <checkers/net/actor_message.hpp>
#include <checkers/helpers/insertion_sort.hpp>

#include <checkers/log/backend.hpp>
#include <timber/logger.hpp>
#include <timber/log.hpp>

#include <wheels/support/hash.hpp>

namespace whirl::checkers {

using namespace commute::transport;

struct Network {
  // Delivery

  // Heap: global
  ActorMessage ExtractMessage(size_t index) {
    auto message = std::move(buffer_.Get(index));
    buffer_.Delete(index);

    return message;
  }

  // Heap: global
  const ActorMessage& LookUpMessage(size_t index) {
    return buffer_.Get(index);
  }

  // Heap: global
  void AddMessages(std::vector<ActorMessage>& new_messages) {
    for (auto& message : new_messages) {
      buffer_.EmplaceBack(std::move(message));
    }
  }

  // Heap: global
  size_t CountMessages() {
    return buffer_.Size();
  }

  // Heap: global
  size_t Hash() const {
    size_t hash = 0;

    const size_t buffer_size = buffer_.Size();
    size_t hashes[buffer_size];
    for (size_t i = 0; i < buffer_size; ++i) {
      hashes[i] = buffer_.Get(i).hash;
    }

    InsertionSort(hashes, hashes + buffer_size);

    for (size_t i = 0; i < buffer_size; ++i) {
      wheels::HashCombine(hash, hashes[i]);
    }

    return hash;
  }

  // Heap: global
  void Reset() {
    buffer_.Clear();
  }

 private:
  Buffer<ActorMessage> buffer_;
};

}  // namespace whirl::checkers
