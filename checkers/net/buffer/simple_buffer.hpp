#pragma once

#include <deque>

namespace whirl::checkers {

template <class T>
class Buffer {
 public:
  void EmplaceBack(T&& value) {
    buffer_.emplace_back(std::move(value));
  }

  T& Get(size_t index) {
    return buffer_[index];
  }

  const T& Get(size_t index) const {
    return buffer_[index];
  }

  void Delete(size_t index) {
    buffer_.erase(buffer_.begin() + index);
  }

  size_t Size() const {
    return buffer_.size();
  }

  void Clear() {
    buffer_.clear();
  }

 private:
  std::deque<T> buffer_;
};

}  // namespace whirl::checkers
