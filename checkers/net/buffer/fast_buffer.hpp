#pragma once

#include <deque>
#include <memory>

#include <checkers/net/buffer/implicit_cartesian.hpp>

namespace whirl::checkers {

template <class T>
class Buffer {
 public:
  Buffer() {
    Clear();
  }

  void EmplaceBack(T&& value) {
    buffer_->InsertAt(buffer_->Size(), std::move(value));
  }

  T& Get(size_t index) {
    return buffer_->GetAt(index);
  }

  void Delete(size_t index) {
    buffer_->Delete(index, index);
  }

  size_t Size() {
    return buffer_->Size();
  }

  void Clear() {
    buffer_.reset(new detail::ImplicitCartesian<T>);
  }

 private:
  std::unique_ptr<detail::ImplicitCartesian<T>> buffer_;
};

}  // namespace whirl::checkers
