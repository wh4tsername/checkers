#pragma once

#include <deque>
#include <random>
#include <iostream>

namespace whirl::checkers::detail {

template <typename T>
class ImplicitCartesian {
 public:
  ImplicitCartesian();

  ~ImplicitCartesian();

  void InsertAt(int position, T&& value);

  void Delete(int left_border, int right_border);

  void Shift(int left_border, int right_border, int shift);

  void Print();

  T& GetAt(int position);

  size_t Size();

 private:
  struct Node {
    T value_;
    int priority_;
    int size_;
    Node* left_;
    Node* right_;

    Node(int priority, T&& value) {
      value_ = std::move(value);
      priority_ = priority;
      size_ = 1;
      left_ = nullptr;
      right_ = nullptr;
    }

    ~Node() {
      delete left_;
      delete right_;
    }
  };

  int GetSize(Node* root);

  void Update(Node* root);

  T& GetAt(Node* root, int position);

  std::pair<Node*, Node*> SplitKth(Node* root, int k_param);

  Node* Merge(Node* first_root, Node* second_root);

  struct Generator {
    // TODO
    Generator() : rng_(42), dist_(1, max_priority) {
    }
    int operator()() {
      return dist_(rng_);
    }

    const int max_priority = 1000000;
    std::mt19937 rng_;
    std::uniform_int_distribution<std::mt19937::result_type> dist_;
  };

  Node* root_;
  Generator generator_;
};

////////////////////////////////////////////////////////////////////////////////

template <typename T>
ImplicitCartesian<T>::ImplicitCartesian() {
  root_ = nullptr;
}

template <typename T>
ImplicitCartesian<T>::~ImplicitCartesian() {
  delete root_;
}

template <typename T>
void ImplicitCartesian<T>::InsertAt(int position, T&& value) {
  std::pair<Node*, Node*> roots = SplitKth(root_, position);
  Node* new_node = new Node(generator_(), std::move(value));
  Node* new_root = Merge(roots.first, new_node);
  root_ = Merge(new_root, roots.second);
}

template <typename T>
void ImplicitCartesian<T>::Delete(int left_border, int right_border) {
  std::pair<Node*, Node*> roots = SplitKth(root_, left_border);
  std::pair<Node*, Node*> split_result =
      SplitKth(roots.second, right_border - left_border + 1);
  delete split_result.first;
  root_ = Merge(roots.first, split_result.second);
}

template <typename T>
void ImplicitCartesian<T>::Shift(int left_border, int right_border, int shift) {
  std::pair<Node*, Node*> roots = SplitKth(root_, left_border);
  std::pair<Node*, Node*> split_result =
      SplitKth(roots.second, right_border - left_border + 1);

  Node* shifted = split_result.first;
  std::pair<Node*, Node*> shifted_roots = SplitKth(shifted, shift);
  shifted = Merge(shifted_roots.second, shifted_roots.first);

  shifted = Merge(roots.first, shifted);
  root_ = Merge(shifted, split_result.second);
}

template <typename T>
void ImplicitCartesian<T>::Print() {
  Print(root_);
  std::cout << std::endl;
}

template <typename T>
T& ImplicitCartesian<T>::GetAt(int position) {
  return GetAt(root_, position + 1);
}

template <typename T>
size_t ImplicitCartesian<T>::Size() {
  return GetSize(root_);
}

template <typename T>
int ImplicitCartesian<T>::GetSize(Node* root) {
  if (root != nullptr) {
    return root->size_;
  }
  return 0;
}

template <typename T>
void ImplicitCartesian<T>::Update(Node* root) {
  if (root == nullptr) {
    return;
  }
  root->size_ = 1 + GetSize(root->left_) + GetSize(root->right_);
}

template <typename T>
T& ImplicitCartesian<T>::GetAt(Node* root, int position) {
  if (position <= GetSize(root->left_)) {
    return GetAt(root->left_, position);
  }
  if (position == GetSize(root->left_) + 1) {
    return root->value_;
  }
  return GetAt(root->right_, position - GetSize(root->left_) - 1);
}

template <typename T>
std::pair<typename ImplicitCartesian<T>::Node*,
          typename ImplicitCartesian<T>::Node*>
ImplicitCartesian<T>::SplitKth(Node* root, int k_param) {
  if (root == nullptr) {
    return {nullptr, nullptr};
  }
  if (k_param <= GetSize(root->left_)) {
    auto res = SplitKth(root->left_, k_param);
    root->left_ = res.second;
    Update(root);
    return {res.first, root};
  }
  auto res = SplitKth(root->right_, k_param - GetSize(root->left_) - 1);
  root->right_ = res.first;
  Update(root);
  return {root, res.second};
}

template <typename T>
typename ImplicitCartesian<T>::Node* ImplicitCartesian<T>::Merge(
    Node* first_root, Node* second_root) {
  if (first_root == nullptr) {
    return second_root;
  }
  if (second_root == nullptr) {
    return first_root;
  }
  if (first_root->priority_ <= second_root->priority_) {
    first_root->right_ = Merge(first_root->right_, second_root);
    Update(first_root);
    return first_root;
  }
  second_root->left_ = Merge(first_root, second_root->left_);
  Update(second_root);
  return second_root;
}

}  // namespace whirl::checkers::detail
