#pragma once

#include <cstdint>
#include <functional>

#include <muesli/serializable.hpp>
#include <commute/rpc/message.hpp>

#include <checkers/net/address.hpp>
#include <checkers/config/server.hpp>

namespace whirl::checkers {

using Message = commute::rpc::Message;

struct Packet {
  ServerConfig from_;
  Port port_;
  Message message_;

  MUESLI_SERIALIZABLE(from_, port_, message_)
};

}  // namespace whirl::checkers
