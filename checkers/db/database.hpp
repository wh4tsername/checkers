#pragma once

#include <whirl/node/db/database.hpp>
#include <whirl/node/db/mutation.hpp>

#include <persist/fs/fs.hpp>
#include <persist/log/reader.hpp>
#include <persist/log/writer.hpp>

#include <timber/logger.hpp>
#include <timber/log.hpp>

#include <wheels/support/hash.hpp>
#include <checkers/helpers/fast_hash.hpp>
#include <checkers/helpers/insertion_sort.hpp>

namespace whirl::checkers::db {

class Database : public node::db::IDatabase {
 public:
  explicit Database(timber::ILogBackend* backend)
      : logger_("Database", backend) {
  }

  virtual ~Database() = default;

  void Open(const std::string& directory) override;

  // Mutate

  void Put(const node::db::Key& key, const node::db::Value& value) override;
  void Delete(const node::db::Key& key) override;

  void Write(node::db::WriteBatch batch) override;

  // Read

  std::optional<node::db::Value> TryGet(
      const node::db::Key& key) const override;

  node::db::ISnapshotPtr MakeSnapshot() override;

  void Clear() {
    entries_.clear();
    hash_ = 0;
  }

  size_t Hash() const {
    return hash_;
  }

 private:
  std::map<node::db::Key, node::db::Value> entries_;
  timber::Logger logger_;

  size_t hash_{0};
};

}  // namespace whirl::checkers::db
