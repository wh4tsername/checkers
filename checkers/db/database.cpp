#include <checkers/db/database.hpp>
#include <checkers/log/bytes.hpp>
#include <timber/log.hpp>

namespace whirl::checkers::db {

void Database::Open([[maybe_unused]] const std::string& directory) {
  // TODO nothing?
}

// Mutate

void Database::Put(const node::db::Key& key, const node::db::Value& value) {
  wheels::HashCombine(hash_, FastHash<node::db::Key>{}(key));
  wheels::HashCombine(hash_, FastHash<node::db::Value>{}(value));

  entries_[key] = value;
}

void Database::Delete(const node::db::Key& key) {
  entries_.erase(key);
}

void Database::Write(node::db::WriteBatch batch) {
  for (const auto& mut : batch.muts) {
    switch (mut.type) {
      case node::db::MutationType::Put:
        LOG_INFO("Put('{}', '{}')", mut.key, log::FormatMessage(*mut.value));
        Put(mut.key, *mut.value);
        break;
      case node::db::MutationType::Delete:
        LOG_INFO("Delete('{}')", mut.key);
        Delete(mut.key);
        break;
    }
  }
}

// Read

std::optional<node::db::Value> Database::TryGet(
    const node::db::Key& key) const {
  auto it = entries_.find(key);
  if (it != entries_.end()) {
    return it->second;
  } else {
    return std::nullopt;
  }
}

node::db::ISnapshotPtr Database::MakeSnapshot() {
  return nullptr;
}

}  // namespace whirl::checkers::db
