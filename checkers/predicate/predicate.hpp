#pragma once

#include <string>

namespace whirl::checkers {

class IPredicate {
 public:
  virtual void Fail(const std::string& reason) = 0;
  virtual void Report(const std::string& state) = 0;

  virtual bool IsCorrect() = 0;
  virtual bool MakesSense() = 0;

  virtual void Reset() = 0;
};

}  // namespace whirl::checkers
