#pragma once

#include <string>
#include <cstdlib>

#include <muesli/serializable.hpp>

namespace whirl::checkers {

using ActorId = size_t;

struct ServerConfig {
  ActorId id;
  std::string hostname;
  std::string pool;

  MUESLI_SERIALIZABLE(id, hostname, pool)
};

}  // namespace whirl::checkers
