#include <checkers/runtime/setup.hpp>

#include <checkers/server/server.hpp>

#include <whirl/runtime/access.hpp>

namespace whirl::checkers {

static node::IRuntime& GetCheckersRuntime() {
  return ThisServer().GetNodeRuntime();
}

void SetupCheckersRuntime() {
  node::SetupRuntime([]() -> node::IRuntime& {
    return GetCheckersRuntime();
  });
}

}  // namespace whirl::checkers
