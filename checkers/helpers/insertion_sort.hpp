#pragma once

#include <algorithm>

namespace whirl {

template <typename It>
inline void InsertionSort(It first, It last) {
  for (auto cur = first; cur != last; ++cur) {
    std::rotate(std::upper_bound(first, cur, *cur), cur, cur + 1);
  }
}

}  // namespace whirl
