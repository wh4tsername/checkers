#pragma once

#include <boost/container_hash/hash.hpp>

namespace whirl {

template <typename T>
struct FastHash;

template <>
struct FastHash<std::string> {
  size_t operator()(const std::string& str) const {
    return hash_(str);
  }

 private:
//  boost::hash<std::string> hash_;
  std::hash<std::string> hash_;
};

}  // namespace whirl
