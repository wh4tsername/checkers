#pragma once

#include <checkers/log/event.hpp>

#include <iostream>

namespace whirl::checkers::log {

void FormatLogEventTo(const Event& event, std::ostream& out);

}  // namespace whirl::checkers::log
