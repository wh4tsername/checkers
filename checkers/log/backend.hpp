#pragma once

#include <checkers/log/event.hpp>
#include <checkers/log/env.hpp>

#include <timber/backend.hpp>

#include <optional>
#include <fstream>
#include <vector>

namespace whirl::checkers::log {

class LogBackend : public timber::ILogBackend {
 public:
  LogBackend();

  // ILogBackend

  // Context: Server
  timber::Level GetMinLevelFor(const std::string& component) const override;

  // Context: Server
  void Log(timber::Event event) override;

  // Checkers

  void AppendToFile(const std::string& path);

 private:
  void Write(const Event& event);

  void InitLevels();

 private:
  LogLevels levels_;

  std::optional<std::ofstream> file_;
};

}  // namespace whirl::checkers::log
