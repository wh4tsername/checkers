#pragma once

#include <string>

namespace whirl::checkers::log {

// Format bytes produced by muesli::Serialize
std::string FormatMessage(const std::string& bytes);

}  // namespace whirl::checkers::log
